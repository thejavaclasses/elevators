package com.mesosphere

import com.mesosphere.elevators.Direction.Direction
import com.mesosphere.elevators.StandingState.StandingState

package object elevators {

  type Floor = Int
  type Millis = Long

  case class Elevator(
    id: Int,
    state: Either[Direction, StandingState],
    currentFloor: Floor,
    tasks: Seq[Floor])

  object Direction extends Enumeration {
    type Direction = Value
    val Up, Down = Value
  }

  object StandingState extends Enumeration {
    type StandingState = Value
    val StandingClosed, StandingOpen = Value
  }
}
