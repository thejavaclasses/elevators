package com.mesosphere.elevators

import akka.actor.{ActorRef, Props, Actor}
import com.mesosphere.elevators.ElevatorController._

/**
 * @author roman.gorodyshcher
 */
class ElevatorController(initialConfig: Elevator, elevatorRuntime: ActorRef) extends Actor {

  private var elevator = initialConfig
  private var isBusy = false

  override def receive: Receive = {
    case FloorRequest(floor) =>
      elevator = elevator.copy(tasks = elevator.tasks :+ floor)
      if (!isBusy) takeNextTask()

    case GetElevatorInfo =>
      sender ! elevator

    case OpenDoorsRequest => elevator.state match {
      case Right(StandingState.StandingClosed) =>
        isBusy = true
        elevatorRuntime ! ElevatorRuntime.OpenDoors
      case _ => //ignore
    }

    case CloseDoorsRequest => elevator.state match {
      case Right(StandingState.StandingOpen) =>
        isBusy = true
        elevatorRuntime ! ElevatorRuntime.CloseDoors
      case _ => //ignore
    }

    case ElevatorRuntime.AtFloor(floor) =>
      elevator = elevator.copy(currentFloor = floor)

    case ElevatorRuntime.CommandComplete(cmd) =>
      isBusy = false
      cmd match {
        case ElevatorRuntime.OpenDoors =>
          elevator = elevator.copy(state = Right(StandingState.StandingOpen))
          elevatorRuntime ! ElevatorRuntime.CloseDoors
          isBusy = true
        case ElevatorRuntime.CloseDoors =>
          elevator = elevator.copy(state = Right(StandingState.StandingOpen))
          takeNextTask()
        case ElevatorRuntime.Move(_, floor) =>
          elevator = elevator.copy(currentFloor = floor, state = Right(StandingState.StandingClosed))
          elevatorRuntime ! ElevatorRuntime.OpenDoors
          isBusy = true
      }
  }

  private[this] def takeNextTask() = {
    elevator = elevator.copy(tasks = elevator.tasks.filterNot(_ == elevator.currentFloor))
    if (elevator.tasks.nonEmpty) {
      //take most recent task to define next moving direction
      //then reorder all tasks so that the elevator can complete as many tasks as possible without changing its direction
      val direction = if (elevator.tasks.head > elevator.currentFloor) Direction.Up else Direction.Down
      val upTasks = elevator.tasks.filter(_ > elevator.currentFloor)
      val downTasks = elevator.tasks.filter(_ < elevator.currentFloor)
      val next::rest = if (direction == Direction.Up) upTasks.sorted ++ downTasks
                       else downTasks.sorted.reverse ++ upTasks
      elevator = elevator.copy(tasks = rest, state = Left(direction))
      elevatorRuntime ! ElevatorRuntime.Move(elevator.currentFloor, next)
      isBusy = true
    }
  }
}

object ElevatorController {
  def props(elevator: Elevator, elevatorRT: ActorRef) = Props(classOf[ElevatorController],  elevator, elevatorRT)

  case class FloorRequest(floor: Floor)

  case object OpenDoorsRequest

  case object CloseDoorsRequest

  case object GetElevatorInfo
}
