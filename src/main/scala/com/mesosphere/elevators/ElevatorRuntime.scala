package com.mesosphere.elevators

import akka.actor.Actor
import com.mesosphere.elevators.ElevatorRuntime._

/**
 * Wrapper over low-level elevator API
 *
 * @author roman.gorodyshcher
 */
class ElevatorRuntime extends Actor {
  override def receive: Receive = {
    case cmd @ Move(start, end) =>
      val step = if (start < end) 1 else -1
      for (i <- start to end by step) {
        doWork()
        sender ! AtFloor(i)
      }
      sender ! CommandComplete(cmd)
    case cmd @ OpenDoors =>
      doWork()
      sender ! CommandComplete(cmd)
    case cmd @ CloseDoors =>
      doWork()
      sender ! CommandComplete(cmd)
  }

  private def doWork() = Thread.sleep(1000)
}

object ElevatorRuntime {
  case class Move(start: Floor, end: Floor)

  case object OpenDoors

  case object CloseDoors

  case class CommandComplete(command: Any)

  case class AtFloor(floor: Floor)
}
