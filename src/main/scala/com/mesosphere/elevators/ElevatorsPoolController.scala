package com.mesosphere.elevators

import akka.actor.{ActorRef, Props, Actor}
import com.mesosphere.elevators.Direction.Direction
import com.mesosphere.elevators.ElevatorsPoolController.{RequestUpdate, PollElevatorsState, ElevatorRequest}
import scala.collection.mutable
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * @author roman.gorodyshcher
 */
class ElevatorsPoolController(elevators: Seq[ActorRef]) extends Actor {

  val states = mutable.HashMap[Int, (Elevator, ActorRef)]()

  elevators foreach (controller => context.system.scheduler.schedule(1 second, 500 milliseconds, self, RequestUpdate(controller)))

  override def receive: Receive = {
    case state @ Elevator(id, _, _, _) =>
      states.put(id, (state, sender()))

    case ElevatorRequest(floor, direction) =>
      //submit to least loaded elevator or to the 1st one if it's a very beginning
      val selected = if (states.nonEmpty) {
        val (_, controller) = states.values.minBy(_._1.tasks.length)
        controller
      } else elevators.head
      selected ! ElevatorController.FloorRequest(floor)

    case PollElevatorsState =>
      sender ! states.values.map(_._1).toList.sortBy(_.id)

    case RequestUpdate(controller) =>
      controller ! ElevatorController.GetElevatorInfo
  }
}

object ElevatorsPoolController {
  def props(controllers: Seq[ActorRef]) = Props(classOf[ElevatorsPoolController],  controllers)

  case class ElevatorRequest(floor: Floor, direction: Direction)

  case object PollElevatorsState

  case class RequestUpdate(controller: ActorRef)
}
