package com.mesosphere.elevators

import scala.io.StdIn
import akka.actor.{ActorRef, Props, ActorSystem}
import java.util.concurrent.Executors
import scala.util.Random
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global


/**
 * @author roman.gorodyshcher
 */
object ElevatorsRunner {

  def main(args: Array[String]): Unit = {
    println("Preparing elevators work simulation")

    println("Set number of floors in the building in a range from 5 to 50:")
    val floors = StdIn.readInt()
    assert((5 to 50).contains(floors))

    println("Set number of elevators in a range from 1 to 16:")
    val elevators = StdIn.readInt()
    assert((1 to 16).contains(elevators))

    println("Set rate of passengers per minute in a range from 1 to 60:")
    val passengers = StdIn.readInt()
    assert((1 to 60).contains(passengers))

    println("Set simulation duration in seconds:")
    val duration = StdIn.readInt()
    assert(duration > 0)

    val (pool, controllers) = instantiateElevatorsSystem(elevators, duration)
    runSimulation(pool, controllers, floors, passengers, duration)
  }

  private def runSimulation(pool: ActorRef, controllers: Seq[ActorRef], floors: Int, passengerRate: Int, duration: Int) = {
    Random.setSeed(System.currentTimeMillis())

    //emulate new passengers at random floors
    executor.scheduleAtFixedRate(new Runnable {
      override def run() = {
        if (Random.nextInt(60) <= passengerRate) {
          val passengerFloor = Random.nextInt(floors + 1)
          val direction = if (passengerFloor == 0) Direction.Up
                          else if (passengerFloor == floors) Direction.Down
                          else Random.shuffle(Direction.values.toList).head
          pool ! ElevatorsPoolController.ElevatorRequest(passengerFloor, direction)
        }
      }
    }, 0, 1, SECONDS)

    //visualize
    executor.scheduleAtFixedRate(new Runnable {
      override def run() = {
        implicit val timeout = Timeout(5 seconds)
        val states = pool ? ElevatorsPoolController.PollElevatorsState

        def floorOrdinalStr(floor: Int) = if (floor % 10 == 1 && floor % 11 != 0) floor + "st"
                                          else if (floor % 10 == 2 && floor % 12 != 0) floor + "nd"
                                          else if (floor % 10 == 3 && floor % 13 != 0) floor + "rd"
                                          else if (floor == 0) floor
                                          else floor + "th"

        states foreach (elevators => {
          elevators.asInstanceOf[Seq[Elevator]].foreach {
            case Elevator(id, Left(direction), floor, tasks) =>
              println(s"Elevator #$id is moving $direction from ${floorOrdinalStr(floor)} floor. Next floors: $tasks")
            case Elevator(id, Right(StandingState.StandingOpen), floor, tasks) =>
              println(s"Elevator #$id opened doors at ${floorOrdinalStr(floor)} floor.")
            case Elevator(id, Right(StandingState.StandingClosed), floor, Seq()) =>
              println(s"Elevator #$id is standing idle at ${floorOrdinalStr(floor)} floor.")
            case Elevator(id, Right(StandingState.StandingClosed), floor, _) =>
              println(s"Elevator #$id closed doors at ${floorOrdinalStr(floor)} floor.")
          }
        })
      }
    }, 2, 2, SECONDS)
  }

  private def instantiateElevatorsSystem(count: Int, ttl: Int) = {
    val system = ActorSystem.create("ElevatorsSimulation")
    executor.schedule(new Runnable {
      override def run() = {
        system.terminate()
        executor.shutdownNow()
      }
    }, ttl, SECONDS)

    val runtimes = for (i <- 1 to count)
      yield (i, system.actorOf(Props[ElevatorRuntime], s"ElelevatorRuntime$i"))

    val elevatorPrototype = Elevator(0, Right(StandingState.StandingClosed), 0, Nil)

    val controllers = runtimes map {
      case (uid, rt) => system.actorOf(ElevatorController.props(elevatorPrototype.copy(id = uid), rt))
    }

    val pool = system.actorOf(ElevatorsPoolController.props(controllers))

    (pool, controllers)
  }

  private val executor = Executors.newSingleThreadScheduledExecutor()
}
