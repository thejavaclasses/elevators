
lazy val root = (project in file(".")).
  settings(
    name := "mesosphere-task",
    version := "1.0.0-SNAPSHOT",
    scalaVersion := "2.11.6",
    mainClass in Compile := Some("com.mesosphere.elevators.ElevatorsRunner")
  )

resolvers ++= Seq(
  "Sbt plugins"                   at "https://dl.bintray.com/sbt/sbt-plugin-releases",
  "Maven Central Server"          at "http://repo1.maven.org/maven2",
  "TypeSafe Repository Releases"  at "http://repo.typesafe.com/typesafe/releases/",
  "TypeSafe Repository Snapshots" at "http://repo.typesafe.com/typesafe/snapshots/",
  "Akka Snapshot Repository"      at "http://repo.akka.io/snapshots/"
)

resolvers += Resolver.mavenLocal

libraryDependencies ++= Seq(
  "com.typesafe.akka"             %% "akka-actor"                     % "2.4-SNAPSHOT",
  "org.scalatest"                 %% "scalatest"                      % "2.2.4"               % "test"
)

parallelExecution in Test := false

