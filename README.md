Elevator Control System
=======================
 
This program emulates elevator control system. 
When the program starts, you will be proposed to specify 4 simulation parameters:

1. number of floors in simulated house
2. number of elevators
3. rate of appearing new passengers per minute
4. the time of simulation in seconds

After parameters specified, the simulation starts. Changes in elevators' states are printed to console.



Installation and Running
------------------------

There are two ways to run the program (both require preinstalled SBT) after you download sources:

1. Navigate to the folder with sources in terminal and execute `sbt run` command.
2. Navigate to the folder with sources and execute the following commands one by one:

        sbt assembly
        cd target/scala-2.11
        java -jar mesosphere-task-assembly-1.0.0-SNAPSHOT.jar
 


Scheduler Algorithm Details 
---------------------------

The scheduling algorithm defines an elevator's pool strategy of choosing exact elevator for handling new task, and elevator's strategy of ordering received tasks for better throughput.

Elevator's pool strategy is simple: new task is submitted to least loaded elevator i.e. to the elevator with the shortest tasks queue.

Elevator's strategy of picking next task from its queue is the following:

1. Take the first task from the queue to define moving direction: if the goal floor is below the elevator - next direction is DOWN, else - UP.
2. Find all tasks from the queue which would lead to the same direction selection.
3. Sort these tasks in an order from closest floor to elevator to the furthest one.
4. Replace the queue with preprocessed tasks in the beginning and the rest task in the end.
5. Pull and execute the 1st task from reordered queue.

Example: suppose an elevator is currently on the 5 floor (out of 10), and the tasks queue contains the following floors to visit: `[2, 4, 9, 7]`.
Then the algorithm will do the following actions on each correspondent step:

1. Take `2` to define, that the next direction is `Down`.
2. Split tasks into groups `[2, 4]` and `[9, 7]`, which corresponds to moving down from the 5th floor and moving up respectively.
3. Sort the group which corresponds to selected direction (`[2, 4]`) so that floors which are closer to 5th floor go first. The result is `[4, 2]`.
4. Replace the queue with `[4, 2, 9, 7]`.
5. Pull and execute the 1st task i.e. `4`, leaving only `[2, 9, 7]` in the queue.

It's easy to see, that the original queue will be processed in the following order by applying the used algorithm: `[4, 2, 7, 9]`.

One more interesting thing: if a newly-added task lays on the current elevator's way - it will be completed right away.
For example, if the `3` is added to the queue after 1st algorithm iteration - the queue will look like `[2, 9, 7, 3]`.
The second iteration will reorder it to `[3, 2, 9, 7]` and will execute `3` next.

**The key feature of this algorithm - it takes at most 2 full scans of floors by elevator to complete all tasks at a queue snapshot.**


Both strategies have a wide room for ad-hoc improvements.
For instance, a pool can ignore new task if it's exact copy of a task which is already in a queue of one of the elevators.
Another example: if there's more than 1 idle elevator, a pool can submit new task the the closest elevator.